/**
 * Constructor to save our Entries.
 * 
 * @param {String} URL 
 */


function Entry(URL){
    this.name = URL;
    this.counter = 0;
    this.increment = function(){
        this.counter++;
    };
    this.setZero = function(){
        this.counter = 0;
    };
}
